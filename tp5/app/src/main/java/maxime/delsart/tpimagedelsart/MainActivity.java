package maxime.delsart.tpimagedelsart;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    static final Integer FIRST_ACTIVITY = 1;

    private Bitmap initialBitmap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView text = findViewById(R.id.textView);
        text.setText("");

        Button loadingButton = findViewById(R.id.button);
        loadingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleOnClickLoadButton();
            }
        });

        Button restoreButton = findViewById(R.id.buttonRestore);
        restoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView imageView = findViewById(R.id.imageView);
                imageView.setImageBitmap(Bitmap.createBitmap(initialBitmap));
            }
        });

        registerForContextMenu(findViewById(R.id.imageView));
    }

    protected void handleOnClickLoadButton() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Selectionner un fichier à afficher"),FIRST_ACTIVITY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == FIRST_ACTIVITY) {
            Uri imageUri = data.getData();

            if (imageUri != null) {
                // ----- preparer les options de chargement de l’image
                BitmapFactory.Options option = new BitmapFactory.Options();
                option.inMutable = true; // l’image pourra être modifiée
                // ------ chargement de l’image - valeur retournee null en cas d’erreur
                Bitmap bm = null;
                try {
                    bm = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri), null, option);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                if (bm != null) {
                    ImageView imageView = findViewById(R.id.imageView);
                    imageView.setImageBitmap(bm);

                    TextView text = findViewById(R.id.textView);
                    text.setText(imageUri.getPath());

                    initialBitmap = Bitmap.createBitmap(bm);
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mirror_horizontal:
                mirrorImage("horizontal");
                break;
            case R.id.mirror_vertical:
                mirrorImage("vertical");
                break;
            case R.id.rotate90Horaire:
                rotate90Horaire();
                break;
            case R.id.rotate90AntiHoraire:
                rotate90AntiHoraire();
                break;
        }
        return true;
    }

    protected void mirrorImage(String type) {
        ImageView imageView = findViewById(R.id.imageView);
        Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
        Bitmap newBitmap = Bitmap.createBitmap(bitmap);

        int height = bitmap.getHeight();
        int width = bitmap.getWidth();

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int pixel = bitmap.getPixel(x, y);

                int newX = x;
                int newY = y;

                if (type.equals("horizontal")) {
                    newX = (width - 1 - x);
                } else if (type.equals("vertical")) {
                    newY = (height - 1 - y);
                }

                newBitmap.setPixel(newX, newY, pixel);
            }
        }

        imageView.setImageBitmap(newBitmap);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.inverseColor:
                inverseColor();
                return true;
            case R.id.imageGris:
                imageGris();
                return true;
                case R.id.imageGris2:
                imageGris2();
                return true;
            case R.id.imageGris3:
                imageGris3();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    protected void inverseColor() {
        ImageView imageView = findViewById(R.id.imageView);
        Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
        Bitmap newBitmap = Bitmap.createBitmap(bitmap);

        int height = bitmap.getHeight();
        int width = bitmap.getWidth();

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int pixel = bitmap.getPixel(x, y);

                int redValue = 255 - Color.red(pixel);
                int blueValue = 255 - Color.blue(pixel);
                int greenValue = 255 - Color.green(pixel);

                int color = Color.argb(255, redValue, greenValue, blueValue);
                newBitmap.setPixel(x, y, color);
            }
        }

        imageView.setImageBitmap(newBitmap);
    }

    protected void imageGris() {
        ImageView imageView = findViewById(R.id.imageView);
        Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
        Bitmap newBitmap = Bitmap.createBitmap(bitmap);

        int height = bitmap.getHeight();
        int width = bitmap.getWidth();

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int pixel = bitmap.getPixel(x, y);

                int redValue = Color.red(pixel);
                int blueValue = Color.blue(pixel);
                int greenValue = Color.green(pixel);

                int gris = (redValue + blueValue + greenValue) / 3;
                int color = Color.argb(255, gris, gris, gris);
                newBitmap.setPixel(x, y, color);
            }
        }

        imageView.setImageBitmap(newBitmap);
    }

    protected void imageGris2() {
        ImageView imageView = findViewById(R.id.imageView);
        Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
        Bitmap newBitmap = Bitmap.createBitmap(bitmap);

        int height = bitmap.getHeight();
        int width = bitmap.getWidth();

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int pixel = bitmap.getPixel(x, y);

                int redValue = Color.red(pixel);
                int blueValue = Color.blue(pixel);
                int greenValue = Color.green(pixel);

                int[] array = { redValue, blueValue, greenValue };
                int min = 255;
                int max = 0;

                for (int num : array) {
                    if (num < min) {
                        min = num;
                    }
                    if (num > max) {
                        max = num;
                    }
                }

                int gris = (min + max) / 2;
                int color = Color.argb(255, gris, gris, gris);
                newBitmap.setPixel(x, y, color);
            }
        }

        imageView.setImageBitmap(newBitmap);
    }

    protected void imageGris3() {
        ImageView imageView = findViewById(R.id.imageView);
        Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
        Bitmap newBitmap = Bitmap.createBitmap(bitmap);

        int height = bitmap.getHeight();
        int width = bitmap.getWidth();

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int pixel = bitmap.getPixel(x, y);

                int gris = (int)((Color.red(pixel) * 0.21) + (Color.blue(pixel) * 0.07) + (Color.green(pixel) * 0.72));
                int color = Color.argb(255, gris, gris, gris);
                newBitmap.setPixel(x, y, color);
            }
        }

        imageView.setImageBitmap(newBitmap);
    }

    protected void rotate90Horaire() {
        ImageView imageView = findViewById(R.id.imageView);
        Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getWidth(), bitmap.getConfig());


        int height = bitmap.getHeight();
        int width = bitmap.getWidth();

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int pixel = bitmap.getPixel(x, y);

                int newX = newBitmap.getWidth() - y - 1;
                int newY = x;

                newBitmap.setPixel(newX, newY, pixel);
            }
        }

        imageView.setImageBitmap(newBitmap);
    }

    protected void rotate90AntiHoraire() {
        ImageView imageView = findViewById(R.id.imageView);
        Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getWidth(), bitmap.getConfig());


        int height = bitmap.getHeight();
        int width = bitmap.getWidth();

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int pixel = bitmap.getPixel(x, y);

                int newX = y;
                int newY = newBitmap.getHeight() - x - 1;

                newBitmap.setPixel(newX, newY, pixel);
            }
        }

        imageView.setImageBitmap(newBitmap);
    }


}