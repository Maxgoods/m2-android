package maxime.delsart.contentproviderdelsart;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.UserDictionary;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Switch;
import android.widget.TextView;

public class UserDictionnary extends AppCompatActivity {

    private String order = "ASC";
    private SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_dictionnary);

        SwitchCompat sw = (SwitchCompat) findViewById(R.id.switch2);
        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    order = "ASC";
                } else {
                    order = "DESC";
                }
                refreshList();
            }
        });

        Button buttonAdd = (Button) findViewById(R.id.buttonAdd);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditText editText = (EditText) findViewById(R.id.editText);
                String value = editText.getText().toString();
                if (value.equals("")) return;

                if (checkIfStringExistInDictionary(value)) {
                    System.out.println("La valeur existe déja.");
                    editText.setText("");
                    return;
                }

                ContentValues mNewValues = new ContentValues();
                mNewValues.put(UserDictionary.Words.APP_ID, "maxime.delsart.contentproviderdelsart");
                mNewValues.put(UserDictionary.Words.LOCALE, "fr_FR");
                mNewValues.put(UserDictionary.Words.WORD, value);
                mNewValues.put(UserDictionary.Words.FREQUENCY, "250");

                getContentResolver().insert(
                        UserDictionary.Words.CONTENT_URI,
                        mNewValues
                );

                editText.setText("");

                refreshList();
            }
        });


        ListView lv = (ListView) findViewById(R.id.listView);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textViewWord = (TextView) view.findViewById(android.R.id.text1);
                String word = textViewWord.getText().toString();

                ContentResolver resolver = getContentResolver();

                String selection = UserDictionary.Words.WORD + " = ?";
                String[] selectionArgs = new String[] { word };

                resolver.delete(UserDictionary.Words.CONTENT_URI, selection, selectionArgs);

                refreshList();
            }
        });

        this.refreshList();
    }

    public void refreshList() {
        ContentResolver resolver = getContentResolver();
        String sortOrder = UserDictionary.Words.WORD + " " + order;

        Cursor cursor = resolver.query(UserDictionary.Words.CONTENT_URI, null, null, null, sortOrder);

        ListView lv = (ListView) findViewById(R.id.listView);
        adapter = new SimpleCursorAdapter(this,
                android.R.layout.two_line_list_item,
                cursor,
                new String[] {
                        UserDictionary.Words.WORD,
                        UserDictionary.Words.FREQUENCY
                },
                new int[] {
                        android.R.id.text1,
                        android.R.id.text2
                },
                0
        );
        lv.setAdapter(adapter);
    }

    public boolean checkIfStringExistInDictionary(String stringToVerify) {
        ContentResolver resolver = getContentResolver();

        String selection = UserDictionary.Words.WORD + " = ?";
        String[] selectionArgs = new String[] { stringToVerify };
        String sortOrder = UserDictionary.Words.WORD + " " + order;

        Cursor cursor = resolver.query(UserDictionary.Words.CONTENT_URI, null, selection, selectionArgs, sortOrder);

        if (cursor.getCount() == 0) {
            return false;
        }
        return true;
    }
}