package maxime.delsart.contentproviderdelsart;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.UserDictionary;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Switch;
import android.widget.TextView;

public class Contacts extends AppCompatActivity {

    private String order = "ASC";
    private SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        SwitchCompat sw = (SwitchCompat) findViewById(R.id.switch2);
        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    order = "ASC";
                } else {
                    order = "DESC";
                }
                refreshList();
            }
        });

        Button buttonAdd = (Button) findViewById(R.id.buttonAdd);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditText editText = (EditText) findViewById(R.id.editText);
                String value = editText.getText().toString();
                if (value.equals("")) return;

                if (checkIfStringExistInDictionary(value)) {
                    System.out.println("La valeur existe déja.");
                    editText.setText("");
                    return;
                }

                ContentValues rawValues = new ContentValues();
                rawValues.put(ContactsContract.RawContacts.ACCOUNT_TYPE, "maxime.delsart.contentproviderdelsart");
                rawValues.put(ContactsContract.RawContacts.ACCOUNT_NAME, value);
                Uri rawContactUri = getContentResolver().insert(ContactsContract.RawContacts.CONTENT_URI, rawValues);
                long rawContactId = ContentUris.parseId(rawContactUri);

                ContentValues dataValues = new ContentValues();
                dataValues.put(ContactsContract.Data.RAW_CONTACT_ID, rawContactId);
                dataValues.put(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, value);
                dataValues.put(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE);

                getContentResolver().insert(ContactsContract.Data.CONTENT_URI, dataValues);

                editText.setText("");

                refreshList();
            }
        });

        ListView lv = (ListView) findViewById(R.id.listView);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textViewWord = (TextView) view.findViewById(android.R.id.text2);
                String contactId = textViewWord.getText().toString();

                String[] selectionArgs = new String[] { contactId };

                getContentResolver().delete(ContactsContract.Data.CONTENT_URI, ContactsContract.Data.RAW_CONTACT_ID + " = ?", selectionArgs);
                getContentResolver().delete(ContactsContract.RawContacts.CONTENT_URI, ContactsContract.RawContacts.CONTACT_ID + " = ?", selectionArgs);

                refreshList();
            }
        });

        this.refreshList();
    }


    public void refreshList() {
        ContentResolver resolver = getContentResolver();
        String sortOrder = ContactsContract.Contacts.DISPLAY_NAME + " " + order;

        Cursor cursor = resolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, sortOrder);

        ListView lv = (ListView) findViewById(R.id.listView);
        adapter = new SimpleCursorAdapter(this,
                android.R.layout.two_line_list_item,
                cursor,
                new String[] {
                        ContactsContract.Contacts.DISPLAY_NAME_PRIMARY,
                        ContactsContract.Contacts._ID,
                },
                new int[] {
                        android.R.id.text1,
                        android.R.id.text2,
                },
                0
        );
        lv.setAdapter(adapter);
    }

    public boolean checkIfStringExistInDictionary(String stringToVerify) {
        ContentResolver resolver = getContentResolver();

        String selection = ContactsContract.Contacts.DISPLAY_NAME_PRIMARY + " = ?";
        String[] selectionArgs = new String[] { stringToVerify };
        String sortOrder = ContactsContract.Contacts.DISPLAY_NAME_PRIMARY + " " + order;

        Cursor cursor = resolver.query(ContactsContract.Contacts.CONTENT_URI, null, selection, selectionArgs, sortOrder);

        if (cursor.getCount() == 0) {
            return false;
        }
        return true;
    }
}