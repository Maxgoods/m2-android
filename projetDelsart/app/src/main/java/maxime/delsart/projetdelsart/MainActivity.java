package maxime.delsart.projetdelsart;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


import java.io.IOException;
import java.io.InputStream;
import java.text.Normalizer;
import java.util.List;
import java.util.Random;


public class MainActivity extends AppCompatActivity {

    Handler handlerUI = new Handler();
    private int errorCharIndex = 0;
    private int actualCharIndex = 0;

    private int testModeErrorCharIndex = 0;
    private int testModeActualCharIndex = 0;

    private static final int delayDisplayInMs = 200;

    private static final int colorGreen =  Color.rgb(0,255,0);
    private static final int colorRed = Color.rgb(255,0,0);
    private static final int colorWhite = Color.rgb(255, 255, 255);

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.aleaPalindrome:
                try {
                    displayRandomText("userPalindromes.txt");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.isPalindrome:
                try {
                    displayRandomText("userNonPalindromes.txt");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.about:
                openAboutPopup();
                break;
        }

        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            createDictionaryFiles();
        } catch (IOException e) {
            e.printStackTrace();
        }

        setEditText("");

        Button cleanButton = findViewById(R.id.cleanButton);
        cleanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editText = findViewById(R.id.editText);
                TextView cleanText = findViewById(R.id.cleanText);

                String editString = editText.getText().toString();
                String cleanedString = cleanString(editString);
                cleanText.setText(cleanedString.toLowerCase());

                TextView returnText = findViewById(R.id.returnText);
                returnText.setText("");
            }
        });


        Button returnButton = findViewById(R.id.returnButton);
        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView cleanText = findViewById(R.id.cleanText);
                TextView returnText = findViewById(R.id.returnText);

                if (cleanText.length() == 0) {
                    returnText.setText("");
                    return;
                }

                String cleanString = cleanText.getText().toString();
                String returnedString = returnString(cleanString);
                returnText.setText(returnedString);
            }
        });

        Button compareButton = findViewById(R.id.compareButton);
        compareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compare();
            }
        });


        Button goToAdmin = findViewById(R.id.goToAdmin);
        goToAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AdminLogin.class);
                startActivityForResult(intent, 1);
            }
        });

        Button testButton = findViewById(R.id.buttonTest);
        testButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testMode();
            }
        });

    }

    private void createDictionaryFiles() throws IOException {
        // On ne peut pas modifier les fichiers ressources de l'application
        // On va donc créer des fichiers modifiable par l'utilisateur en se basant sur le contenu des fichiers ressources
        Context context = this.getApplicationContext();

        int palindromeId = getResources().getIdentifier("palindromes", "raw", this.getPackageName());
        InputStream inputPalindrome = context.getResources().openRawResource(palindromeId);
        List<String> allPalindromes = FileHelpers.getListFromResourcesFile(inputPalindrome);
        FileHelpers.writeListInUserFile(allPalindromes, "userPalindromes.txt", context);

        int nonPalindromeId = getResources().getIdentifier("nonpalindromes", "raw", this.getPackageName());
        InputStream inputNonPalindrome = context.getResources().openRawResource(nonPalindromeId);
        List<String> allNonPalindromes = FileHelpers.getListFromResourcesFile(inputNonPalindrome);
        FileHelpers.writeListInUserFile(allNonPalindromes, "userNonPalindromes.txt", context);

    }

    private static String cleanString(String text) {
        String normalizedString = Normalizer.normalize(text, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
        return normalizedString.replaceAll("[^a-zA-Z]", "");
    }

    private static String returnString(String text) {
        StringBuilder returnedString = new StringBuilder();
        for(int i = text.length(); i > 0; i--){
            returnedString.append(text.charAt(i - 1));
        }

        return returnedString.toString();
    }

    private void setEditText(String texte) {
        EditText editText = findViewById(R.id.editText);
        editText.setText(texte);

        TextView cleanText = findViewById(R.id.cleanText);
        cleanText.setText("");

        TextView returnText = findViewById(R.id.returnText);
        returnText.setText("");
    }

    private void displayRandomText(String filePath) throws IOException {
        List<String> allText = FileHelpers.getListFromUserFile(getFilesDir(), filePath);

        Random rand = new Random();
        String randomText = allText.get(rand.nextInt(allText.size()));
        setEditText(randomText);
    }

    private void compare() {
        resetUi();

        TextView cleanText = findViewById(R.id.cleanText);
        String cleanString = cleanText.getText().toString();

        TextView returnText = findViewById(R.id.returnText);
        String returnString = returnText.getText().toString();

        if (cleanString.equals("") || returnString.equals("")) return;

        for(int i = 0; i < cleanText.length(); i++ ){
            if (cleanString.charAt(i) != returnString.charAt(i)) {
                errorCharIndex = i;
                break;
            }
        }

        if (errorCharIndex == -1) {
            // Si toutes les lettres sont bonnes
            handleUiCorrectPalindrome();
        } else {
            // Sinon, il y a une erreur qq part
            handleUiWrongPalindrome();
        }

    }

    private void handleUiCorrectPalindrome() {
        TextView cleanText = findViewById(R.id.cleanText);
        String cleanString = cleanText.getText().toString();

        TextView returnText = findViewById(R.id.returnText);

        SpannableString cleanSpannable = new SpannableString(cleanText.getText());
        SpannableString returnSpannable = new SpannableString(returnText.getText());

        Runnable r = new Runnable() {
            @Override
            public void run() {
                if (actualCharIndex < cleanString.length()) {
                    updateColorSpannable(
                            cleanSpannable,
                            0,
                            actualCharIndex + 1,
                            colorGreen
                    );
                    updateColorSpannable(
                            returnSpannable,
                            0,
                            actualCharIndex + 1,
                            colorGreen
                    );

                    actualCharIndex++;
                    handlerUI.postDelayed(this, delayDisplayInMs);
                }
                cleanText.setText(cleanSpannable);
                returnText.setText(returnSpannable);
            }
        };
        handlerUI.postDelayed(r, delayDisplayInMs);
    }

    private void handleUiWrongPalindrome() {
        TextView cleanText = findViewById(R.id.cleanText);
        TextView returnText = findViewById(R.id.returnText);

        SpannableString cleanSpannable = new SpannableString(cleanText.getText());
        SpannableString returnSpannable = new SpannableString(returnText.getText());

        Runnable r = new Runnable() {
            @Override
            public void run() {
                // On met du vert tant qu'on a pas atteint l'erreur
                if (actualCharIndex < errorCharIndex){
                    updateColorSpannable(
                            cleanSpannable,
                            0,
                            actualCharIndex + 1,
                            colorGreen
                    );
                    updateColorSpannable(
                            returnSpannable,
                            0,
                            actualCharIndex + 1,
                            colorGreen
                    );

                    actualCharIndex++;
                    handlerUI.postDelayed(this, delayDisplayInMs);
                } else {
                    // C'est l'erreur, on la met en rouge

                    updateColorSpannable(
                            cleanSpannable,
                            errorCharIndex,
                            errorCharIndex + 1,
                            colorRed
                    );
                    updateColorSpannable(
                            returnSpannable,
                            errorCharIndex,
                            errorCharIndex + 1,
                            colorRed
                    );

                }
                cleanText.setText(cleanSpannable);
                returnText.setText(returnSpannable);
            }
        };
        handlerUI.postDelayed(r, delayDisplayInMs);

    }

    private void testMode() {
        resetUi();

        TextView cleanText = findViewById(R.id.cleanText);
        String cleanString = cleanText.getText().toString();

        if (cleanString.equals("")) return;

        int cleanLength = cleanString.length() - 1;
        for (int i = 0; i < cleanText.length(); i++ ) {
            if (cleanString.charAt(i) != cleanString.charAt(cleanLength - i) ) {
                testModeErrorCharIndex = i;
                break;
            }
        }

        SpannableString cleanSpannable = new SpannableString(cleanText.getText());

        // Si testModeErrorCharIndex n'est pas modifié, tout est bon
        if (testModeErrorCharIndex == -1) {
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    if(testModeActualCharIndex <= cleanLength){
                        updateColorSpannable(
                                cleanSpannable,
                                0,
                                testModeActualCharIndex + 1,
                                colorGreen
                        );

                        updateColorSpannable(
                                cleanSpannable,
                                cleanLength - testModeActualCharIndex,
                                cleanLength + 1,
                                colorGreen
                        );

                        testModeActualCharIndex++;
                        handlerUI.postDelayed(this, delayDisplayInMs);
                    }
                    cleanText.setText(cleanSpannable);
                }
            };
            handlerUI.postDelayed(r, delayDisplayInMs);
        } else {
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    if (testModeActualCharIndex < testModeErrorCharIndex) {
                        updateColorSpannable(
                                cleanSpannable,
                                0,
                                testModeActualCharIndex + 1,
                                colorGreen
                        );

                        updateColorSpannable(
                                cleanSpannable,
                                cleanLength - testModeActualCharIndex,
                                cleanLength + 1,
                                colorGreen
                        );

                        testModeActualCharIndex++;
                        handlerUI.postDelayed(this, delayDisplayInMs);
                    } else {

                        updateColorSpannable(
                                cleanSpannable,
                                testModeErrorCharIndex,
                                testModeErrorCharIndex + 1,
                                colorRed
                        );
                        updateColorSpannable(
                                cleanSpannable,
                                cleanLength - testModeErrorCharIndex,
                                cleanLength - testModeErrorCharIndex + 1,
                                colorRed
                        );

                    }
                    cleanText.setText(cleanSpannable);
                }
            };
            handlerUI.postDelayed(r, delayDisplayInMs);
        }
    }

    private void updateColorSpannable(SpannableString spannable, int indexStart, int indexEnd, int color) {
        spannable.setSpan(
                new BackgroundColorSpan(color),
                indexStart,
                indexEnd,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        );
    }

    private void resetUi() {
        actualCharIndex = 0;
        errorCharIndex = -1;
        testModeActualCharIndex = 0;
        testModeErrorCharIndex = -1;

        TextView cleanText = findViewById(R.id.cleanText);
        TextView returnText = findViewById(R.id.returnText);
        SpannableString cleanSpannable = new SpannableString(cleanText.getText());
        SpannableString returnSpannable = new SpannableString(returnText.getText());

        updateColorSpannable(
                cleanSpannable,
                0,
                cleanText.length(),
                colorWhite
        );
        updateColorSpannable(
                returnSpannable,
                0,
                returnText.length(),
                colorWhite
        );

        cleanText.setText(cleanSpannable);
        returnText.setText(returnSpannable);
    }

    private void openAboutPopup() {
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MainActivity.this);
        final LayoutInflater inflater = getLayoutInflater();
        final View view = inflater.inflate(R.layout.about_popup, null);

        alertBuilder.setTitle("A propos de ...")
            .setView(view)
            .setPositiveButton("OK", (dialog, which) -> {});

        Dialog popup = alertBuilder.create();
        popup.show();
    }

    public static Boolean isPalindrome(String texte) {
        Boolean isPalindrome = true;

        String cleanString = cleanString(texte);
        String returnString = returnString(cleanString);

        for (int i = 0; i < cleanString.length(); i++ ) {
            if (cleanString.charAt(i) != returnString.charAt(i)) {
                isPalindrome = false;
                break;
            }
        }

        return isPalindrome;
    }
}