package maxime.delsart.projetdelsart;

import android.content.Context;
import android.util.Log;

import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class FileHelpers {

    public static void writeListInUserFile(List<String> list, String file, Context context) {
        String data = "";
        for (String s : list) {
            data += s + "\r\n";
        }

        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
                    context.openFileOutput(
                            file,
                            Context.MODE_PRIVATE
                    ), StandardCharsets.ISO_8859_1
            );
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }

    }

    // Resource file = files in raw directory, we can't edit theses files
    public static List<String> getListFromResourcesFile(InputStream inputStream) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.ISO_8859_1));
        List data = new ArrayList();

        String line;
        while ((line = br.readLine()) != null) {
            data.add(line);
        }

        return data;
    }

    // User file = files which can be edited
    public static List<String> getListFromUserFile(File filesDir, String filePath) {
        List data = new ArrayList();

        File userFile = new File(filesDir, filePath);
        try {
            data = new ArrayList<String>(FileUtils.readLines(userFile, StandardCharsets.ISO_8859_1));
        } catch (IOException e) {
            System.out.println(e);
        }

        return data;
    }
}
