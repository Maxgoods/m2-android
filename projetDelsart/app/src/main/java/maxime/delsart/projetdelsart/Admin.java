package maxime.delsart.projetdelsart;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

public class Admin extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        Button adminRetour = findViewById(R.id.adminRetour);
        adminRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent(Admin.this, AdminLogin.class);
                setResult(Admin.RESULT_CANCELED, returnIntent);
                finish();
            }
        });

        Button addPalindrome = findViewById(R.id.addPalindrome);
        addPalindrome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editTextPalindrome = findViewById(R.id.editTextPalindrome);
                addToPalindrome(editTextPalindrome.getText().toString());
            }
        });

        Button addNonPalindrome = findViewById(R.id.addNonPalindrome);
        addNonPalindrome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editTextNonPalindrome = findViewById(R.id.editTextNonPalindrome);
                addToNonPalindrome(editTextNonPalindrome.getText().toString());
            }
        });

    }

    private void addToPalindrome(String texte) {
        if (texte.equals("")) return;

        Boolean isPalindrome = MainActivity.isPalindrome(texte);
        System.out.println(isPalindrome);
        if (!isPalindrome) return;

        List<String> palindromes = FileHelpers.getListFromUserFile(getFilesDir(), "userPalindromes.txt");
        palindromes.add(texte);
        FileHelpers.writeListInUserFile(palindromes, "userPalindromes.txt", this.getApplicationContext());

        EditText editTextPalindrome = findViewById(R.id.editTextPalindrome);
        editTextPalindrome.setText("");
    }

    private void addToNonPalindrome(String texte) {
        if (texte.equals("")) return;

        List<String> nonPalindromes = FileHelpers.getListFromUserFile(getFilesDir(), "userNonPalindromes.txt");
        nonPalindromes.add(texte);
        FileHelpers.writeListInUserFile(nonPalindromes, "userNonPalindromes.txt", this.getApplicationContext());

        EditText editTextNonPalindrome = findViewById(R.id.editTextNonPalindrome);
        editTextNonPalindrome.setText("");
    }


}