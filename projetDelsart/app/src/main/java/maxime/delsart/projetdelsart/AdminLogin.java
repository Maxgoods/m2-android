package maxime.delsart.projetdelsart;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class AdminLogin extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);

        Button loginButton = findViewById(R.id.login);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText editPassword = findViewById(R.id.editPassword);
                if (editPassword.getText().toString().equals("perec")) {
                    Intent intent = new Intent(AdminLogin.this, Admin.class);
                    startActivityForResult(intent, 1);
                } else {
                    TextView loginError = findViewById(R.id.loginError);
                    loginError.setVisibility(View.VISIBLE);
                }
            }
        });

        Button adminLoginRetour = findViewById(R.id.adminLoginRetour);
        adminLoginRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent(AdminLogin.this, MainActivity.class);
                setResult(AdminLogin.RESULT_CANCELED, returnIntent);
                finish();
            }
        });
    }
}