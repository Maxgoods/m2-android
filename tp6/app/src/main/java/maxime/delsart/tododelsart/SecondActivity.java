package maxime.delsart.tododelsart;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent intent = getIntent();

        Button buttonCancel = (Button) findViewById(R.id.buttonCancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent(SecondActivity.this, MainActivity.class);
                setResult(SecondActivity.RESULT_CANCELED, returnIntent);
                finish();
            }
        });


        Button buttonAdd = (Button) findViewById(R.id.buttonAdd);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TextView editText = (TextView) findViewById(R.id.editText);

                Intent returnIntent = new Intent(SecondActivity.this, MainActivity.class);
                returnIntent.putExtra("result", editText.getText().toString());
                setResult(SecondActivity.RESULT_OK, returnIntent);
                finish();
            }
        });
    }
}