package maxime.delsart.tododelsart;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import maxime.delsart.tododelsart.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private ArrayList<String> todoList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setSupportActionBar(binding.toolbar);

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                startActivityForResult(intent, 1);
            }
        });

        this.initListViewContent();
    }

    private void initListViewContent() {
        File filesDir = getFilesDir();
        File todoFile = new File(filesDir, "todo.txt");
        try {
            todoList = new ArrayList<String>(FileUtils.readLines(todoFile));
        } catch (IOException e) {
            todoList = new ArrayList<String>();
        }

        this.refreshListViewContent();
    }

    private void refreshListViewContent() {
        ListView lv = (ListView) findViewById(R.id.listView);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_checked,
                todoList
        );
        lv.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.menuDelete) {
            ListView lv = (ListView) findViewById(R.id.listView);
            SparseBooleanArray checkedItems = lv.getCheckedItemPositions();
            List<String> itemsToDelete = new ArrayList<String>() {};

            for (int i = 0; i < checkedItems.size(); i++) {
                String stringToDelete = todoList.get(checkedItems.keyAt(i));
                itemsToDelete.add(stringToDelete);
            }

            for (String stringToDelete : itemsToDelete) {
                todoList.remove(stringToDelete);
            }

            this.refreshListViewContent();

            return true;
        } else if (id == R.id.menuSave) {
            this.saveSelectedItems();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == MainActivity.RESULT_OK){
                String result = data.getStringExtra("result");

                if(result.trim().equals("")){
                    return;
                }

                todoList.add(result);
                this.refreshListViewContent();
                return;
            }

            if (resultCode == SecondActivity.RESULT_CANCELED) {
                System.out.println("Canceled");
            }
        }
    }

    private void saveSelectedItems() {
        Context context = this.getApplicationContext();
        String data = "";
        for (String s : todoList) {
            data += s + "\r\n";
        }

        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
                    context.openFileOutput(
                            "todo.txt",
                            Context.MODE_PRIVATE
                    )
            );
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
}