package maxime.delsart.tpimagedelsart;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileNotFoundException;

public class MainActivity extends AppCompatActivity {
    static final Integer FIRST_ACTIVITY = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView text = findViewById(R.id.textView);
        text.setText("");

        Button loadingButton = findViewById(R.id.button);
        loadingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleOnClickLoadButton();
            }
        });

        Button horizontalMirrorButton = findViewById(R.id.button2);
        horizontalMirrorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleOnClickHorizontalMirrorButton();
            }
        });

        Button verticalMirrorButton = findViewById(R.id.button4);
        verticalMirrorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleOnClickVerticalMirrorButton();
            }
        });
    }

    protected void handleOnClickLoadButton() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Selectionner un fichier à afficher"),FIRST_ACTIVITY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == FIRST_ACTIVITY) {
            Uri imageUri = data.getData();

            if (imageUri != null) {
                // ----- preparer les options de chargement de l’image
                BitmapFactory.Options option = new BitmapFactory.Options();
                option.inMutable = true; // l’image pourra être modifiée
                // ------ chargement de l’image - valeur retournee null en cas d’erreur
                Bitmap bm = null;
                try {
                    bm = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri), null, option);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                if (bm != null) {
                    ImageView imageView = findViewById(R.id.imageView);
                    imageView.setImageBitmap(bm);

                    TextView text = findViewById(R.id.textView);
                    text.setText(imageUri.getPath());

                    findViewById(R.id.button2).setEnabled(true);
                    findViewById(R.id.button4).setEnabled(true);
                }
            }
        }
    }

    protected void handleOnClickHorizontalMirrorButton() {
        rotateImage("horizontal");

    }

    protected void handleOnClickVerticalMirrorButton() {
        rotateImage("vertical");
    }

    protected void rotateImage(String type) {
        ImageView imageView = findViewById(R.id.imageView);
        Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
        Bitmap newBitmap = Bitmap.createBitmap(bitmap);

        int height = bitmap.getHeight();
        int width = bitmap.getWidth();

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int pixel = bitmap.getPixel(x, y);

                int newX = x;
                int newY = y;

                if (type.equals("horizontal")) {
                    newX = (width - 1 - x);
                } else if (type.equals("vertical")) {
                    newY = (height - 1 - y);
                }

                newBitmap.setPixel(newX, newY, pixel);
            }
        }

        imageView.setImageBitmap(newBitmap);
    }
}