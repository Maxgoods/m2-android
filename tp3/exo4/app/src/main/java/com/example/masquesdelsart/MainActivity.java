package com.example.masquesdelsart;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    static final Integer SECOND_ACTIVITY = 1;
    static final Integer THIRD_ACTIVITY = 2;
    static final Integer RESULT_OK = 1;
    static final Integer RESULT_CANCELED = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView textLastname = findViewById(R.id.textLastname);
        TextView textFirstname = findViewById(R.id.textFirstname);
        TextView textTel = findViewById(R.id.textTel);

        Button buttonGoToSecond = findViewById(R.id.buttonGoToSecond);
        buttonGoToSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(MainActivity.this, SecondActivity.class);
                newIntent.putExtra("lastname",  textLastname.getText());
                newIntent.putExtra("firstname",  textFirstname.getText());
                newIntent.putExtra("tel",  textTel.getText());
                startActivityForResult(newIntent, SECOND_ACTIVITY);

            }
        });

        TextView textAdresseNum = findViewById(R.id.textAdresseNum);
        TextView textAdresseRue = findViewById(R.id.textAdresseRue);
        TextView textAdresseCP = findViewById(R.id.textAdresseCP);
        TextView textAdresseVille = findViewById(R.id.textAdresseVille);

        Button buttonGoToThird = findViewById(R.id.buttonGoToThird);
        buttonGoToThird.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(MainActivity.this, ThirdActivity.class);
                newIntent.putExtra("adresseNum",  textAdresseNum.getText());
                newIntent.putExtra("adresseRue",  textAdresseRue.getText());
                newIntent.putExtra("adresseCP",  textAdresseCP.getText());
                newIntent.putExtra("adresseVille",  textAdresseVille.getText());
                startActivityForResult(newIntent, THIRD_ACTIVITY);

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SECOND_ACTIVITY && resultCode == RESULT_OK) {
            TextView textLastname = findViewById(R.id.textLastname);
            TextView textFirstname = findViewById(R.id.textFirstname);
            TextView textTel = findViewById(R.id.textTel);

            textLastname.setText(data.getStringExtra("lastname"));
            textFirstname.setText(data.getStringExtra("firstname"));
            textTel.setText(data.getStringExtra("tel"));
        }

        if (requestCode == THIRD_ACTIVITY && resultCode == RESULT_OK) {
            TextView textAdresseNum = findViewById(R.id.textAdresseNum);
            TextView textAdresseRue = findViewById(R.id.textAdresseRue);
            TextView textAdresseCP = findViewById(R.id.textAdresseCP);
            TextView textAdresseVille = findViewById(R.id.textAdresseVille);

            textAdresseNum.setText(data.getStringExtra("adresseNum"));
            textAdresseRue.setText(data.getStringExtra("adresseRue"));
            textAdresseCP.setText(data.getStringExtra("adresseCP"));
            textAdresseVille.setText(data.getStringExtra("adresseVille"));
        }
    }
}
