package com.example.masquesdelsart;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ThirdActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        EditText editTextAdresseNum = findViewById(R.id.editTextAdresseNum);
        EditText editTextAdresseRue = findViewById(R.id.editTextAdresseRue);
        EditText editTextAdresseCP = findViewById(R.id.editTextAdresseCP);
        EditText editTextAdresseVille = findViewById(R.id.editTextAdresseVille);



        Intent intent = getIntent();
        editTextAdresseNum.setText(intent.getStringExtra("adresseNum"));
        editTextAdresseRue.setText(intent.getStringExtra("adresseRue"));
        editTextAdresseCP.setText(intent.getStringExtra("adresseCP"));
        editTextAdresseVille.setText(intent.getStringExtra("adresseVille"));

        Button buttonEditAdresse = findViewById(R.id.buttonEditAdresse);
        buttonEditAdresse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("adresseNum", editTextAdresseNum.getText().toString());
                returnIntent.putExtra("adresseRue", editTextAdresseRue.getText().toString());
                returnIntent.putExtra("adresseCP", editTextAdresseCP.getText().toString());
                returnIntent.putExtra("adresseVille", editTextAdresseVille.getText().toString());
                setResult(MainActivity.RESULT_OK, returnIntent);
                finish();
            }
        });

        Button buttonCancelAdresse = findViewById(R.id.buttonCancelAdresse);
        buttonCancelAdresse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                setResult(MainActivity.RESULT_CANCELED, returnIntent);
                finish();
            }
        });
    }
}