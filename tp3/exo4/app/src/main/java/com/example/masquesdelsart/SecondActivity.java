package com.example.masquesdelsart;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        EditText editTextLastname = findViewById(R.id.editTextLastname);
        EditText editTextFirstname = findViewById(R.id.editTextFirstname);
        EditText editTextTel = findViewById(R.id.editTextTel);


        Intent intent = getIntent();
        editTextLastname.setText(intent.getStringExtra("lastname"));
        editTextFirstname.setText(intent.getStringExtra("firstname"));
        editTextTel.setText(intent.getStringExtra("tel"));

        Button buttonEdit = findViewById(R.id.buttonEdit);
        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("lastname", editTextLastname.getText().toString());
                returnIntent.putExtra("firstname", editTextFirstname.getText().toString());
                returnIntent.putExtra("tel", editTextTel.getText().toString());
                setResult(MainActivity.RESULT_OK, returnIntent);
                finish();
            }
        });

        Button buttonCancel = findViewById(R.id.buttonCancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                setResult(MainActivity.RESULT_CANCELED, returnIntent);
                finish();
            }
        });
    }
}