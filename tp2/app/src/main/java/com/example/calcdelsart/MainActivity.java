package com.example.calcdelsart;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // uncomment this line to see application with LinearLayout instead of ConstraintLayout
        // setContentView(R.layout.activity_linear);

        setContentView(R.layout.activity_constraint);

        final Button buttonQuit = findViewById(R.id.buttonQuit);
        final Button buttonRaz = findViewById(R.id.buttonRaz);
        final Button buttonEqual = findViewById(R.id.buttonEqual);
        final TextView textViewResultat = findViewById(R.id.textViewResultat);
        final RadioGroup radioGroup = findViewById(R.id.radioGroup);
        final RadioButton radioButtonPlus = findViewById(R.id.radioButtonPlus);
        final RadioButton radioButtonMoins = findViewById(R.id.radioButtonMoins);
        final RadioButton radioButtonMultiplie = findViewById(R.id.radioButtonMultiplie);
        final RadioButton radioButtonDivise = findViewById(R.id.radioButtonDivise);
        final EditText editText1 = findViewById(R.id.editTextNumber);
        final EditText editText2 = findViewById(R.id.editTextNumber2);

        buttonQuit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finishAndRemoveTask();
            }
        });

        buttonRaz.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                textViewResultat.setText("Résultat");
                editText1.setText("");
                editText2.setText("");
                radioGroup.clearCheck();
                buttonEqual.setEnabled(false);
            }
        });

        buttonEqual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int number1 = Integer.parseInt(editText1.getText().toString());
                int number2 = Integer.parseInt(editText2.getText().toString());
                int radioButtonId = radioGroup.getCheckedRadioButtonId();
                int radioButtonIdPlus = radioButtonPlus.getId();
                int radioButtonIdMoins = radioButtonMoins.getId();
                int radioButtonIdMultiplie = radioButtonMultiplie.getId();
                int radioButtonIdDivise = radioButtonDivise.getId();

                double resultat = 0;

                if (radioButtonId == radioButtonIdPlus) {
                    resultat = number1 + number2;
                } else if (radioButtonId == radioButtonIdMoins) {
                    resultat = number1 - number2;
                } else if (radioButtonId == radioButtonIdMultiplie) {
                    resultat = number1 * number2;
                    resultat = (double) Math.round(resultat * 100) / 100;
                } else if (radioButtonId == radioButtonIdDivise) {
                    if (number2 != 0) {
                        resultat = (double) number1 / (double) number2;
                        resultat = (double) Math.round(resultat * 100) / 100;
                    }
                }
                textViewResultat.setText("Résultat = " + resultat);
            }
        });

        editText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                checkIfButtonEqualIsEnabled();
            }
        });

        editText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                checkIfButtonEqualIsEnabled();
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                checkIfButtonEqualIsEnabled();
            }
        });
    }

    protected void checkIfButtonEqualIsEnabled() {
        final Button buttonEqual = findViewById(R.id.buttonEqual);
        final RadioGroup radioGroup = findViewById(R.id.radioGroup);
        final EditText editText1 = findViewById(R.id.editTextNumber);
        final EditText editText2 = findViewById(R.id.editTextNumber2);

        Editable text1 = editText1.getText();
        Editable text2 = editText2.getText();
        int radioButtonId = radioGroup.getCheckedRadioButtonId();

        if (text1.length() == 0 || text2.length() == 0 || radioButtonId == -1) {
            buttonEqual.setEnabled(false);
            return;
        }

        buttonEqual.setEnabled(true);
    }
}